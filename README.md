# TorrentCalc
This is a simple little Javascript library that takes a network upload speed as
input and returns an object containing recommended BitTorrent speed settings as
output.

Special thanks to [The 8472] for their original work at
http://infinite-source.de/az/az-calc.html

```js
import torrentCalc from 'torrentcalc';

const settings = torrentCalc(12.41)['Mbps'];

console.log(settings);

//	{
// 		maxUploadSpeed: '75000 Kbps',
//		maxDownloadSpeed: '0 Kbps',
//		maxActiveTorrents: 19,
//		maxSimultaneousDownloads: 13,
//		averageUploadSpeedPerSlot: '241.02 Kbps',
//		maxSlotsPerTorrent: 16,
//		maxConnectionsGlobally: 1200,
//		maxConnectionsPerTorrent: 76
//	}
```
