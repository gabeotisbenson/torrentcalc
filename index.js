const getValues = uploadSpeed => {
	let maxUploadSpeed = Math.floor(uploadSpeed * 0.8);
	let maxDownloadSpeed = 0;
	const maxActiveTorrents = Math.round(Math.pow(maxUploadSpeed / 4, 0.3));
	const maxSimultaneousDownloads = Math.ceil(maxActiveTorrents * 2 / 3);
	let averageUploadSpeedPerSlot = Math.max(Math.pow(maxUploadSpeed / 3.5, 0.55), 4).toFixed(2);
	const maxSlotsPerTorrent = uploadSpeed === 0 ? 0 : Math.floor(maxUploadSpeed / (maxActiveTorrents * averageUploadSpeedPerSlot));
	const maxConnectionsGlobally = Math.round(Math.min(Math.pow(uploadSpeed * 8, 0.8) + 50, 1200));
	const maxConnectionsPerTorrent = Math.round(Math.min(maxConnectionsGlobally * 1.2 / maxActiveTorrents, maxConnectionsGlobally));

	maxUploadSpeed += ' Kbps';
	maxDownloadSpeed += ' Kbps';
	averageUploadSpeedPerSlot += ' Kbps';

	return {
		maxUploadSpeed,
		maxDownloadSpeed,
		maxActiveTorrents,
		maxSimultaneousDownloads,
		averageUploadSpeedPerSlot,
		maxSlotsPerTorrent,
		maxConnectionsGlobally,
		maxConnectionsPerTorrent
	};
};

module.exports = val => {
	return {
		Kbps: getValues(val * 0.125),
		KBps: getValues(val),
		Mbps: getValues(val * 125),
		MBps: getValues(val * 1000),
		Gbps: getValues(val * 125000),
		GBps: getValues(val * 1000000)
	};
};
